from libqtile.config import Key, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import qtile
from groups import groups

mod = "mod4" # Super key

terminal = "alacritty"
browser = "firefox"

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_sglit(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.screen.toggle_group(), desc="Toggle between windows"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "m",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # rofi
    Key([mod], "d", lazy.spawn("rofi -show drun -theme gruvbox-dark"), desc="Rofi application launcher"),
    Key([mod], "c", lazy.spawn("rofi -show run -theme gruvbox-dark"), desc="Rofi application launcher"),

    Key([mod], "f", lazy.spawn(browser), desc="open browser"),
    Key([mod], "o", lazy.spawn("pcmanfm"), desc="Open file manager"),

    # Volume control 
    Key([mod], "minus", lazy.spawn("pulsemixer --change-volume -5"), desc="Decrease the volume"),
    Key([mod], "equal", lazy.spawn("pulsemixer --change-volume +5"), desc="Increase the volume"),
    Key([mod], "0", lazy.spawn("pulsemixer --toggle-mute"), desc="Mute Toggle"),

    Key([mod], "z", lazy.spawn("zathura"), desc="Open zathura pdf viewer"),
    Key([mod], "x", lazy.spawn("xournalpp"), desc="Open xournalpp"),

    # Scrot
    Key(
        [mod],
        "s",
        lazy.spawn(
            "scrot -s /tmp/%Y-%m-%d_%H%M%S.png -q 100 -z --line mode=edge,width=4,color='orange' -e 'xclip -selection clipboard -t image/png -i $f'"
        )
    ),
    Key([mod, "shift"], "s", lazy.spawn(
            [
                "sh",
                "-c",
                "scrot -q 100 -z ~/Images/Screenshots/%Y-%m-%d_%H%M%S.png -e 'xclip -selection clipboard -t image/png -i $f'"
            ]
        )
    ),
]

for i, group in enumerate(groups, 1):
    name = group.name
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name, switch_group=True)))

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]
