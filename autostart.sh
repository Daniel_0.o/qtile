#!/bin/sh
#feh --bg-max ~/.dotfiles/wallpaper.jpg & 
xmodmap -e "keycode 43 = h H h H Left" # h
xmodmap -e "keycode 44 = j J j J Down" # j
xmodmap -e "keycode 45 = k K k K Up" # k
xmodmap -e "keycode 46 = l L l L Right" # l

xmodmap -e "keycode 24 = q Q q Q dead_tilde"
xmodmap -e "keycode 25 = w W w W dead_grave"
