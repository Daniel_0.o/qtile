import psutil
import socket
from libqtile import widget, bar
from datetime import datetime, timedelta, timezone, tzinfo

# A widget that display whether your pc has internet access or not
class InternetConn(widget.base.ThreadPoolText):
    orientations = widget.base.ORIENTATION_HORIZONTAL
    defaults = [
        ("update_interval", 1, "The update interval."),
    ]

    def __init__(self, **config):
        widget.base.ThreadPoolText.__init__(self, "", **config)
        self.add_defaults(InternetConn.defaults)

    def poll(self):
        try:
            stats = psutil.net_if_addrs()
            if stats:
                for interface in stats:
                    for address in stats[interface]:
                        if address.family == 2:  # Check for IPv4 address family
                            if not address.address.startswith("127."):
                                return address.address
            return "disconnected"
        except:
            return "ERROR"

# Better clock
class Clock(widget.Clock):
    def poll(self):
        if self.timezone:
            now = datetime.now(timezone.utc).astimezone(self.timezone)
        else:
            now = datetime.now(timezone.utc).astimezone()

        date = (now + self.DELTA)       
        fmt = date.strftime

        week_day = fmt("%a").capitalize()
        month_name = fmt('%b').capitalize()
        day = fmt("%d")
        year = fmt("%Y").capitalize()

        clock = fmt("%H:%M")

        return f"{month_name} {week_day} {day} {year}, {clock}"

## Modified version that has centered text
class PulseVolume(widget.PulseVolume):
    def __init__(self, **config):
        self.minimium_width = 50
        width = config["width"]
        if (width is not None and width < self.minimium_width) or not width:
           config["width"] = self.minimium_width    

        super().__init__(**config)
        self.max_padding = self.actual_padding
   
    def draw(self):
        self.drawer.clear(self.background or self.bar.background)
        
        self.drawer.ctx.save()
    
        size = self.bar.height if self.bar.horizontal else self.bar.width

        self.layout.draw(
            ## Center the text
            (self.width  - self.layout.width) / 2,
            int(size / 2.0 - self.layout.height / 2.0) + 1,
        )

        self.drawer.draw(
            offsetx=self.offsetx,
            offsety=self.offsety,
            width=self.width,
            height=self.height
        )
