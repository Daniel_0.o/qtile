from libqtile.config import Group

max_groups_count = 9

groups = [ 
    "dev",
    "web",
    "view"
]

groups.extend(''.join([''.join(str(v) for v in range(len(groups) + 1, max_groups_count + 1))]))

groups = [Group(i) for i in groups]
