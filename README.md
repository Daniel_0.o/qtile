## A repo for saving my qtile configuration

![Setup preview](./screenshots/pic.png)

### Information

### Qtile version?

0.26.0

### Os?

[NixOS](https://nixos.org) :snowflake:

### Terminal?

[Alacritty](https://alacritty.org)


### Colorscheme?

[PastelCube](https://gist.github.com/CarlosDanielMaturano/84f5fcd557b39b57fa392d386cc91d50)

### Font?
[FiraCode](https://www.programmingfonts.org/#firacode)
