from libqtile import layout, qtile, widget, bar, hook
from libqtile.config import Group, Match, Screen
from libqtile.lazy import lazy
import os 
import subprocess

from keys import keys 
from colors import colors
from groups import groups
import custom_widgets as cw

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.Popen([home])


## Default width for border
width=2

layouts = [
    layout.MonadTall(
        border_width=width,
        border_focus=colors[5],
        border_normal=colors[2],
    )
]

widget_defaults = dict(
    font="Fira Mono Nerd Font Mono Medium",
    fontsize=18,
    padding=3,
    foreground=colors[5],
    linewidth=2
)

def limit_title_size(title):
    max_title_size = 30
    title_len = len(title)
    if title_len > max_title_size:
        return title[:max_title_size] + '...'
    return title

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
        [
            widget.GroupBox(
                highlight_method="line",
                highlight_color=[colors[0]] * 2,
                rounded=False,
                active=colors[5],
                inactive=colors[2],
                this_current_screen_border=colors[10],
                disable_drag = True,
            ),                
            widget.Sep(padding=20),
            widget.WindowName(parse_text=limit_title_size),
            cw.Clock(),
            widget.Spacer(length=bar.STRETCH ),
            widget.Sep(padding=20),
            cw.InternetConn(),
            widget.Sep(padding=20),
            cw.PulseVolume(
                scroll=False,
                width = 10,
            ),
            widget.Sep(padding=20),
            widget.QuickExit(
                padding=4,
                default_text='[X]',
                countdown_format='[{}]'
            ),
            widget.Systray(),
        ],
        30,
        background=colors[0],
        border_width=[2, 0, 2, 0],  
        border_color=[colors[0]] * 4
        ),
    ),
]

dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    border_focus=colors[4],
    border_normal=colors[3],
    border_width=width,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

wmname = "LG3D"
